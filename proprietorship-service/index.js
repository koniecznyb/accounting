const { loggerMiddleware, serverStartup } = require('common-library');
const app = require('express')();
const https = require('https');
const http = require('http');

const { SERVER_PORT: port } = process.env;
const invoiceService = require('./db.js');

const handleGetRequest = req => invoiceService.getProprietorships(req);
const handlePostRequest = req => invoiceService.saveProprietorship(req.body);
const sendResponse = res => action => (data) => {
  action(data)
    .then(result => res.send(result))
    .catch(() => res.status(500).end());
};

app.use(loggerMiddleware);
app.get('/proprietorships', (req, res) => sendResponse(res)(handleGetRequest)(req));
app.post('/proprietorships', (req, res) => sendResponse(res)(handlePostRequest)(req));

serverStartup(http, https, app)('0.0.0.0', port, 'Proprietorship service');
