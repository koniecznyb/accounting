const pgp = require('pg-promise')();

const dbUrl = process.env.DB_URL;

if (dbUrl == null) {
  throw new Error('DB_URL environment variable is null');
}

const db = pgp(process.env.DB_URL);

const handleResponse = (data) => {
  console.log(`Result is ${JSON.stringify(data)}`);
  return data;
};

const handleError = (error) => {
  console.log(`Error while executing DB statement: ${error}`);
  return error;
};

module.exports = {
  getProprietorships() {
    return db.many('SELECT * FROM proprietorship')
      .then(handleResponse)
      .catch(handleError);
  },
  saveProprietorship(proprietorship) {
    console.log(`Adding new proprietorship entry: ${JSON.stringify(proprietorship)}`);
    return db.none(`INSERT INTO proprietorship 
    (nip, creation_date, last_modification_date, company_name, first_name, main_address, regon, surname) values (
      ${proprietorship.nip},
      current_date,
      current_date,
      '${proprietorship.company_name}',
      '${proprietorship.first_name}',
      '${proprietorship.main_address}',
       ${proprietorship.regon},
      '${proprietorship.surname}' )`)
      .catch(handleError);
  },
};
