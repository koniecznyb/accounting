const express = require('express');
const session = require('express-session');
const connectRedis = require('connect-redis');
const redis = require('redis');
const bcrypt = require('bcrypt');
const Sequelize = require('sequelize');
const http = require('http');
const https = require('https');
const { loggerMiddleware, serverStartup } = require('common-library');

const RedisStore = connectRedis(session);

const {
  SERVER_PORT,
  NODE_ENV,
  REDIS_HOST,
  REDIS_PORT,
  DB_URL,
} = process.env;

const sequelize = new Sequelize(DB_URL);
sequelize
  .authenticate()
  .then(() => {
    console.log('DB Connection successful');
  })
  .catch((err) => {
    console.log(err);
  });

const User = sequelize.define('user', {
  NIP: {
    type: Sequelize.INTEGER,
    unique: true,
    allowNull: false,
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
    allowNull: false,
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});

User.beforeCreate(user => bcrypt.hash(user.password, 10, (hash) => {
  // eslint-disable-next-line no-param-reassign
  user.password = hash;
}));

const client = redis.createClient({
  host: REDIS_HOST,
  port: REDIS_PORT,
});

client.on('error', (err) => {
  console.error(`Error: ${err}`);
});


const app = express();
app.use(session({
  store: new RedisStore({
    client,
  }),
  name: 'sessionId',
  secret: 'test',
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    secure: false,
  },
}));

if (NODE_ENV === 'production') {
  app.set('trust proxy', 1);
  session.cookie.secure = true;
}

app.use(loggerMiddleware);
app.post('/customer', (req, res) => {
  console.log(`Persising user: ${req.body}`);
  res.send('hi');
});

serverStartup(http, https, app)('0.0.0.0', SERVER_PORT, 'Authentication service');
