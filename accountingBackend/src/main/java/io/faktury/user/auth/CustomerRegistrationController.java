package io.faktury.user.auth;

import com.google.common.collect.Sets;
import io.faktury.role.Role;
import io.faktury.role.RoleRepository;
import io.faktury.user.Customer;
import io.faktury.user.CustomerRepository;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by SG0224958 on 29.12.16.
 */
@RepositoryRestController
@Slf4j
public class CustomerRegistrationController {

    @NonNull
    private final CustomerRepository customerRepository;

    @NonNull
    private final RoleRepository roleRepository;

    @NonNull
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public CustomerRegistrationController(CustomerRepository customerRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.customerRepository = customerRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @RequestMapping(value = "/customers", method = RequestMethod.POST)
    public ResponseEntity<Customer> createNewAccount(@RequestBody CustomerSignupDTO customerSignupDTO) {
        Role roleUser = roleRepository.findOne(1L);

        Customer customer = new Customer();
        customer.setRoles(Sets.newHashSet(roleUser));
        customer.setFirstName(customerSignupDTO.getFirstName());
        customer.setLastName("empty");
        customer.setEmail(customerSignupDTO.getEmail());

        String encodedPassword = passwordEncoder.encode(customerSignupDTO.getPassword());
        customer.setPassword(encodedPassword);

        try{
            customerRepository.save(customer);
            return new ResponseEntity<>(customer, HttpStatus.OK);
        } catch (Exception e){
            log.error(e.getMessage());
            return ResponseEntity.badRequest().build();
        }
    }

}
