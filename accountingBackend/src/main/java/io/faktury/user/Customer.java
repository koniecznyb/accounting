package io.faktury.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.faktury.common.CommonEntity;
import io.faktury.role.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "customer")
public class Customer extends CommonEntity {

    @Id
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(joinColumns = @JoinColumn(name = "email", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "role_id", nullable = false))
    @JsonIgnore
    private Set<Role> roles = new HashSet<>();

    @JsonIgnore
    @Column(name = "password", nullable = false)
    private String password;
}

