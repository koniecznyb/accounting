package io.faktury.user;

import io.faktury.user.auth.UserUtils;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.sql.SQLException;
import java.util.Optional;

@RestController
@Slf4j
public class CustomerController {

    @NonNull
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public Principal checkCustomer(Principal user) {
        log.info("Returning user [{}] ", user);
        return user;
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/customer/{firstName}", method = RequestMethod.GET)
    public ResponseEntity<Customer> getUser(@PathVariable String firstName) {
        Optional<Customer> customerOptional = customerRepository.findByFirstName(firstName);
        if (!customerOptional.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Customer customer = customerOptional.get();


        if (!firstName.equals(UserUtils.getCurrentUserName())) {
            throw new AccessDeniedException("Insufficient permissions");
        }

        return new ResponseEntity<>(customer, HttpStatus.OK);
    }

    @ExceptionHandler({SQLException.class, DataAccessException.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Data access error")
    public void databaseError() {
    }
}
