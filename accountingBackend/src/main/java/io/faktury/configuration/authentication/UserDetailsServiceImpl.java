package io.faktury.configuration.authentication;

import io.faktury.role.Role;
import io.faktury.user.Customer;
import io.faktury.user.CustomerRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@Slf4j
public class UserDetailsServiceImpl implements UserDetailsService {

    @NonNull
    private final CustomerRepository customerRepository;

    @Autowired
    public UserDetailsServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        log.debug("Looking for username [{}]", username);
        Optional<Customer> customerOptional = customerRepository.findByFirstName(username);
        if (!customerOptional.isPresent()) {
            throw new UsernameNotFoundException("Not found " + username);
        }
        Customer customer = customerOptional.get();

        log.debug("Found user [{}]", customerOptional);
        String password = customer.getPassword();
        Set<Role> roles = customer.getRoles();

        log.debug("Loading user [{}], with password [{}], with roles [{}]", username, password, roles);

        return new User(username, password, roles);
    }
}
