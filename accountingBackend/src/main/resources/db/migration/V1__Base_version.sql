--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1 (Debian 11.1-1.pgdg90+1)
-- Dumped by pg_dump version 11.1 (Debian 11.1-1.pgdg90+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: customer; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.customer (
    email character varying(255) NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    last_modification_date timestamp without time zone NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    proprietorship_nip bigint
);


ALTER TABLE public.customer OWNER TO accounting;

--
-- Name: customer_roles; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.customer_roles (
    email character varying(255) NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.customer_roles OWNER TO accounting;

--
-- Name: invoice; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.invoice (
    invoice_id bigint NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    last_modification_date timestamp without time zone NOT NULL,
    sum bigint,
    from_nip bigint,
    to_nip bigint
);


ALTER TABLE public.invoice OWNER TO accounting;

--
-- Name: invoice_products; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.invoice_products (
    invoice_id bigint NOT NULL,
    product_id bigint NOT NULL
);


ALTER TABLE public.invoice_products OWNER TO accounting;

--
-- Name: product; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.product (
    product_id bigint NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    last_modification_date timestamp without time zone NOT NULL,
    pkwiu character varying(255) NOT NULL,
    measure_type character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    number integer NOT NULL,
    percentage_vat integer NOT NULL,
    price_vat bigint NOT NULL,
    price_with_vat bigint NOT NULL,
    quantity integer NOT NULL
);


ALTER TABLE public.product OWNER TO accounting;

--
-- Name: proprietorship; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.proprietorship (
    nip bigint NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    last_modification_date timestamp without time zone NOT NULL,
    company_name character varying(255) NOT NULL,
    first_name character varying(255) NOT NULL,
    main_address character varying(255),
    regon bigint NOT NULL,
    surname character varying(255) NOT NULL
);


ALTER TABLE public.proprietorship OWNER TO accounting;

--
-- Name: proprietorship_additional_addresses; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.proprietorship_additional_addresses (
    proprietorship_nip bigint NOT NULL,
    additional_addresses character varying(255)
);


ALTER TABLE public.proprietorship_additional_addresses OWNER TO accounting;

--
-- Name: role; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.role (
    role_id bigint NOT NULL,
    role_name character varying(255) NOT NULL
);


ALTER TABLE public.role OWNER TO accounting;

--
-- Name: schedule; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.schedule (
    schedule_id bigint NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    last_modification_date timestamp without time zone NOT NULL
);


ALTER TABLE public.schedule OWNER TO accounting;

--
-- Name: seq_invoice_id; Type: SEQUENCE; Schema: public; Owner: accounting
--

CREATE SEQUENCE public.seq_invoice_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_invoice_id OWNER TO accounting;

--
-- Name: seq_product_id; Type: SEQUENCE; Schema: public; Owner: accounting
--

CREATE SEQUENCE public.seq_product_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_product_id OWNER TO accounting;

--
-- Name: seq_role_id; Type: SEQUENCE; Schema: public; Owner: accounting
--

CREATE SEQUENCE public.seq_role_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.seq_role_id OWNER TO accounting;

--
-- Name: trip; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.trip (
    trip_id bigint NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    last_modification_date timestamp without time zone NOT NULL
);


ALTER TABLE public.trip OWNER TO accounting;

--
-- Name: vehicle; Type: TABLE; Schema: public; Owner: accounting
--

CREATE TABLE public.vehicle (
    vehicle_id bigint NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    last_modification_date timestamp without time zone NOT NULL
);


ALTER TABLE public.vehicle OWNER TO accounting;

--
-- Name: customer customer_pkey; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (email);


--
-- Name: customer_roles customer_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.customer_roles
    ADD CONSTRAINT customer_roles_pkey PRIMARY KEY (email, role_id);


--
-- Name: invoice invoice_pkey; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT invoice_pkey PRIMARY KEY (invoice_id);


--
-- Name: invoice_products invoice_products_pkey; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.invoice_products
    ADD CONSTRAINT invoice_products_pkey PRIMARY KEY (invoice_id, product_id);


--
-- Name: product product_pkey; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (product_id);


--
-- Name: proprietorship proprietorship_pkey; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.proprietorship
    ADD CONSTRAINT proprietorship_pkey PRIMARY KEY (nip);


--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (role_id);


--
-- Name: schedule schedule_pkey; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.schedule
    ADD CONSTRAINT schedule_pkey PRIMARY KEY (schedule_id);


--
-- Name: trip trip_pkey; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.trip
    ADD CONSTRAINT trip_pkey PRIMARY KEY (trip_id);


--
-- Name: proprietorship uk_ggg67foiorsu93bybo4ona80l; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.proprietorship
    ADD CONSTRAINT uk_ggg67foiorsu93bybo4ona80l UNIQUE (regon);


--
-- Name: invoice_products uk_lnf1nog21tiq933fvt3y66yhd; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.invoice_products
    ADD CONSTRAINT uk_lnf1nog21tiq933fvt3y66yhd UNIQUE (product_id);


--
-- Name: vehicle vehicle_pkey; Type: CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.vehicle
    ADD CONSTRAINT vehicle_pkey PRIMARY KEY (vehicle_id);


--
-- Name: customer_roles fk6jcf0ued4skjxo97h7s2l2s8o; Type: FK CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.customer_roles
    ADD CONSTRAINT fk6jcf0ued4skjxo97h7s2l2s8o FOREIGN KEY (role_id) REFERENCES public.role(role_id);


--
-- Name: invoice fkfxw2umvylnlmbikj54xm2f7r; Type: FK CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT fkfxw2umvylnlmbikj54xm2f7r FOREIGN KEY (to_nip) REFERENCES public.proprietorship(nip);


--
-- Name: proprietorship_additional_addresses fkieb2itagmqynyd65o0fe9ksgl; Type: FK CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.proprietorship_additional_addresses
    ADD CONSTRAINT fkieb2itagmqynyd65o0fe9ksgl FOREIGN KEY (proprietorship_nip) REFERENCES public.proprietorship(nip);


--
-- Name: customer_roles fkkdplp69pp6ge8h4tf8megvjgg; Type: FK CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.customer_roles
    ADD CONSTRAINT fkkdplp69pp6ge8h4tf8megvjgg FOREIGN KEY (email) REFERENCES public.customer(email);


--
-- Name: invoice fkl10o8vq7360r71hwxh7ksj5h3; Type: FK CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.invoice
    ADD CONSTRAINT fkl10o8vq7360r71hwxh7ksj5h3 FOREIGN KEY (from_nip) REFERENCES public.proprietorship(nip);


--
-- Name: customer fkp44t459nm7l5ts3n9dbpyj884; Type: FK CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT fkp44t459nm7l5ts3n9dbpyj884 FOREIGN KEY (proprietorship_nip) REFERENCES public.proprietorship(nip);


--
-- Name: invoice_products fkswm7ncqaw00f90kmrwijr85re; Type: FK CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.invoice_products
    ADD CONSTRAINT fkswm7ncqaw00f90kmrwijr85re FOREIGN KEY (product_id) REFERENCES public.product(product_id);


--
-- Name: invoice_products fksyku4sthayo3edqg4haewq87b; Type: FK CONSTRAINT; Schema: public; Owner: accounting
--

ALTER TABLE ONLY public.invoice_products
    ADD CONSTRAINT fksyku4sthayo3edqg4haewq87b FOREIGN KEY (invoice_id) REFERENCES public.invoice(invoice_id);


--
-- PostgreSQL database dump complete
--