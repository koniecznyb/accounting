insert into proprietorship (nip, creation_date, last_modification_date, company_name, first_name, main_address, regon, surname) VALUES (9691622455, current_date, current_date, 'IT Consulting', 'Bart', '44-123 City 12', '1233456791', 'Koon');

insert into proprietorship (nip, creation_date, last_modification_date, company_name, first_name, main_address, regon, surname) VALUES (9691622456, current_date, current_date, 'Important Company', 'Name', '44-351 Village 3', '123123123', 'Surname');

insert into proprietorship (nip, creation_date, last_modification_date, company_name, first_name, main_address, regon, surname) VALUES (9691622457, current_date, current_date, 'Janusz soft', 'Janusz', '44-233 Metro 1', '123123122', 'Januszewski');

insert into invoice (invoice_id, issue_date, creation_date, last_modification_date, sum, from_nip, to_nip) values (nextval('seq_invoice_id'),'2018-01-01 20:13'  ,current_date, current_date, 12321, 9691622455, 9691622456);

insert into invoice (invoice_id, issue_date, creation_date, last_modification_date, sum, from_nip, to_nip) values (nextval('seq_invoice_id'),'2018-10-08 20:13'  ,current_date, current_date, 1231, 9691622455, 9691622456);

insert into invoice (invoice_id, issue_date, creation_date, last_modification_date, sum, from_nip, to_nip) values (nextval('seq_invoice_id'),'2018-10-09 20:13'  ,current_date, current_date, 123123, 9691622455, 9691622456);

insert into invoice (invoice_id, issue_date, creation_date, last_modification_date, sum, from_nip, to_nip) values (nextval('seq_invoice_id'),'2018-11-11 20:13'  ,current_date, current_date, 53453, 9691622455, 9691622456);

insert into invoice (invoice_id, issue_date, creation_date, last_modification_date, sum, from_nip, to_nip) values (nextval('seq_invoice_id'),'2018-11-11 20:13'  ,current_date, current_date, 324, 9691622455, 9691622456);

insert into invoice (invoice_id, issue_date, creation_date, last_modification_date, sum, from_nip, to_nip) values (nextval('seq_invoice_id'),'2018-12-12 20:13'  ,current_date, current_date, 5435345, 9691622455, 9691622456);

insert into role values (1, 'ROLE_USER');

INSERT INTO customer (email, creation_date, first_name, last_modification_date, last_name, password, proprietorship_nip) VALUES ('a', '2017-02-17 20:27:09.107', 'a', '2017-02-17 20:27:09.107', 'empty', '$2a$10$A9XMBEWUxxv4EK0iWhopEOx.Pg1ZO29xcNLK94TUdoObxcGkgEcfW', 9691622455);

INSERT INTO customer_roles values ('a', 1);
