const amqplib = require('amqplib');

const { createLogger, format, transports } = require('winston');

const { AMQP_BROKER_HOST: amqpBrokerHost } = process.env;

const myFormat = format.printf((
  {
    level,
    message,
    timestamp,
    label,
  },
) => `[${timestamp}]${label} ${level}: ${message}`);

const {
  timestamp,
  label,
  colorize,
  combine,
} = format;

const getLogger = (service, requestId) => createLogger({
  format: combine(
    colorize(),
    label({
      label: `${requestId ? `[${requestId}]` : ''}`,
    }),
    timestamp(),
    myFormat,
  ),
  transports: [new transports.Console()],
});


let connection;

const createExchange = async (exchange) => {
  if (!connection) {
    connection = await amqplib.connect(`amqp://${amqpBrokerHost}`);
  }
  try {
    const channel = await connection.createChannel(exchange);
    await channel.assertExchange(exchange, 'fanout', { durable: false });
    return channel;
  } catch (err) {
    getLogger()
      .error(`AMQP connection failed ${err}`);
  }
  throw Error('Connection to the broker failed');
};


const consumeMessage = async (queue, handler) => {
  const exchange = await createExchange(queue, amqpBrokerHost);
  const q = await exchange.assertQueue('', { exclusive: true });
  exchange.bindQueue(q.queue, queue, '');
  exchange.consume(q.queue, handler);
};

const publishMessage = async (exchangeName, message, logger) => {
  logger.info(`Publishing to [${exchangeName}]: [${message}]`);
  const exchange = await createExchange(exchangeName, amqpBrokerHost);
  return exchange.publish(exchangeName, '', Buffer.from(message));
};

module.exports = {
  getLogger,
  consumeMessage,
  publishMessage,
};
