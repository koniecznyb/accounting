import React from 'react';
import './app.css';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { path } from 'ramda';
import { Route, Switch, withRouter } from 'react-router-dom';
import LoginForm from '../../components/login-form';
import Content from '../content/content';
import { authenticate, loggedIn } from '../../actions/actions';
import { LOADING_SCREEN, LOGIN_FAILURE } from '../../constants/routes';
import LoadingScreen from '../../components/loading-screen';
import LoginFailure from '../../components/login-failure';

const CheckingLogon = () => <div>Checking your login status...</div>;

const InitialComponent = ({
  isCheckingLogon, isLoggedIn, authenticateUser, user, logout,
}) => {
  if (isCheckingLogon) {
    return <CheckingLogon />;
  }
  return isLoggedIn
    ? <Content user={user} logout={logout} />
    : <LoginForm authenticateUser={authenticateUser} />;
};

const AppComponent = props => (
  <div className="App">
    <Switch>
      <Route exact path={LOADING_SCREEN} component={LoadingScreen} />
      <Route exact path={LOGIN_FAILURE} component={() => LoginFailure(props)} />
      <Route path="*" component={() => InitialComponent(props)} />
    </Switch>
  </div>
);

InitialComponent.propTypes = {
  isCheckingLogon: PropTypes.bool.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  authenticateUser: PropTypes.func.isRequired,
  user: PropTypes.string.isRequired,
  logout: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isLoggedIn: path(['LoggedInReducer', 'isLoggedIn'])(state) || false,
  isCheckingLogon: path(['LoggedInReducer', 'isCheckingLogon'])(state) || false,
  error: path(['LoggedInReducer', 'error'])(state),
});

const mapDispatchToProps = dispatch => ({
  isUserLoggedIn: () => dispatch(loggedIn()),
  authenticateUser: (user, password) => {
    dispatch(authenticate(user, password));
  },
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppComponent));
