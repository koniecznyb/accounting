import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { PropTypes } from 'prop-types';

const NewInvoice = (props) => {
  const { handleSubmit } = props;

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor="NIP">NIP</label>
        <Field name="nip" component="input" type="number" />
      </div>
    </form>
  );
};

NewInvoice.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default reduxForm(NewInvoice);
