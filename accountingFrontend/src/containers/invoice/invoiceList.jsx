import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { path } from 'ramda';
import { NavLink } from 'react-router-dom';
import ReactTable from 'react-table';
import { fetchInvoices as fetchInvoicesAction } from '../../actions/actions';
import 'react-table/react-table.css';

class InvoiceList extends Component {
  componentDidMount() {
    const { fetchInvoices, selectedDate } = this.props;
    fetchInvoices(selectedDate);
  }

  render() {
    const { invoices } = this.props;
    return (
      <div>
        <NavLink to="/invoice/new">
          Add new invoice
        </NavLink>
        <ReactTable
          noDataText="No invoices in selected accounting period"
          data={invoices}
          columns={[
            {
              Header: 'Details',
              columns: [{
                Header: 'From NIP',
                accessor: 'from_nip',
              },
              {
                Header: 'To NIP',
                accessor: 'to_nip',
              }],
            },
            {
              Header: 'Date',
              columns: [{
                Header: 'Issuance date',
                accessor: 'issue_date',
              },
              {
                Header: 'Added at',
                accessor: 'creation_date',
              },
              {
                Header: 'Last change',
                accessor: 'last_modification_date',
              }],
            },
            {
              Header: 'Number',
              columns: [{
                Header: 'Total cost',
                accessor: 'sum',
              }],
            },
          ]}
          defaultPageSize={20}
          className="-striped -highlight"
        />
      </div>
    );
  }
}

InvoiceList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  invoices: PropTypes.array,
  fetchInvoices: PropTypes.func.isRequired,
  selectedDate: PropTypes.string.isRequired,
};

InvoiceList.defaultProps = {
  invoices: [],
};

const mapDispatchToProps = dispatch => ({
  fetchInvoices: selectedDate => dispatch(fetchInvoicesAction(selectedDate)),
});

const mapStateToProps = state => (
  {
    selectedDate: path(['AccountingPeriodReducer', 'selectedDate'])(state),
    invoices: path(['InvoiceReducer', 'invoices'])(state),
  });

export default connect(mapStateToProps, mapDispatchToProps)(InvoiceList);
