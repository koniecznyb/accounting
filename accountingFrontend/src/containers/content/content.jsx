import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { path } from 'ramda';
import { connect } from 'react-redux';
import Header from '../../components/header';
import Main from '../main/main';
import { selectAccountingPeriod as selectAccountingPeriodAction, logout as logoutAction } from '../../actions/actions';
import AccountingPeriodSelector from '../../components/accounting-period-selector';

const Content = ({
  user, logout, selectedDate, selectAccountingPeriod,
}) => {
  const handleSubmit = (e) => {
    e.preventDefault();
    logout();
  };

  return (
    <div>
      <div>
        <p>
          Hello
          {user.name}
        </p>
        <button onClick={handleSubmit} type="submit">
          Log out
        </button>
        <AccountingPeriodSelector
          selectedDate={selectedDate}
          selectAccountingPeriod={selectAccountingPeriod}
        />

      </div>
      <Header />
      <Main />
    </div>
  );
};

Content.propTypes = {
  user: PropTypes.shape({}).isRequired,
  logout: PropTypes.func.isRequired,
  selectedDate: PropTypes.string.isRequired,
  selectAccountingPeriod: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  user: path(['LoggedInReducer', 'user'])(state),
  selectedDate: path(['AccountingPeriodReducer', 'selectedDate'])(state),
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logoutAction()),
  selectAccountingPeriod: selectedDate => dispatch(selectAccountingPeriodAction(selectedDate)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Content));
