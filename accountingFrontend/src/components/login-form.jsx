import React from 'react';
import { withRouter } from 'react-router';
import { PropTypes } from 'prop-types';
import { LOADING_SCREEN } from '../constants/routes';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  render() {
    const { username, password } = this.state;

    const handleUsernameChange = (event) => {
      this.setState({ username: event.target.value });
    };
    const handlePasswordChange = (event) => {
      this.setState({ password: event.target.value });
    };
    const onSubmit = (event) => {
      event.preventDefault();
      const { history, authenticateUser } = this.props;
      history.push(LOADING_SCREEN);
      authenticateUser(username, password);
    };
    return (
      <div>
        <h1>Enter your username and password</h1>
        <form onSubmit={onSubmit}>
          <label>
            Username
            <input
              type="text"
              name="username"
              autoComplete="username"
              value={username}
              onChange={handleUsernameChange}
            />
          </label>
          <label>
            Password
            <input
              type="password"
              id="password"
              name="password"
              autoComplete="current-password"
              value={password}
              onChange={handlePasswordChange}
            />
          </label>
          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

LoginForm.propTypes = {
  history: PropTypes.shape({}).isRequired,
  authenticateUser: PropTypes.func.isRequired,
};

export default withRouter(LoginForm);
