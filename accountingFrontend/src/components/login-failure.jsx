import React from 'react';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { ROOT } from '../constants/routes';

const LoginFailure = () => (
  <div>
    <p>
        There was a problem with the login system.
        Please
      {' '}
      <Link to={ROOT}>try again</Link>
.
    </p>
  </div>
);


export default withRouter(LoginFailure);
