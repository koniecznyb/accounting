import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import './navEntry.css';

const activeStyle = {
  textDecoration: 'none',
  color: 'red',
};

const NavEntry = ({ to, children }) => (
  <div className="nav-entry">
    <NavLink to={to} activeStyle={activeStyle}>
      {children}
    </NavLink>
  </div>
);

NavEntry.propTypes = {
  children: PropTypes.string.isRequired,
  to: PropTypes.string.isRequired,
};

export default NavEntry;
