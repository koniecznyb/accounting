import React from 'react';
import NavEntry from './navEntry';
import './navigation.css';

const Navigation = () => (
  <div className="navigation-bar">
    <NavEntry to="/dashboard">Dashboard</NavEntry>
    <NavEntry to="/invoices">Invoices</NavEntry>
    <NavEntry to="/about">About</NavEntry>
  </div>
);

export default Navigation;
