import React from 'react';

const LoadingScreen = () => (
  <div className="loadingScreen">
    Loading...
  </div>
);

export default LoadingScreen;
