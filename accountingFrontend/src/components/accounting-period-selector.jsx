import React from 'react';
import PropTypes from 'prop-types';

const AccountingPeriodSelector = ({ selectedDate, selectAccountingPeriod }) => (
  <div>
    <label>
    Accounting period:
      <input
        type="month"
        id="start"
        name="start"
        min="2000-01"
        value={selectedDate}
        onChange={event => selectAccountingPeriod(event.target.value)}
      />
    </label>
  </div>
);

AccountingPeriodSelector.propTypes = {
  selectedDate: PropTypes.string.isRequired,
  selectAccountingPeriod: PropTypes.func.isRequired,
};

export default AccountingPeriodSelector;
