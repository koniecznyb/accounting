export const LOADING_SCREEN = '/loading-screen';
export const ROOT = '/';
export const DASHBOARD = '/dashboard';
export const LOGIN_FAILURE = '/login-failure';
