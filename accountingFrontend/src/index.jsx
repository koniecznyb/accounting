/* eslint-disable no-undef */
import React from 'react';
import { render } from 'react-dom';
import { applyMiddleware, createStore } from 'redux';
import { combineReducers, install } from 'redux-loop';
import { composeWithDevTools } from 'redux-devtools-extension';
import { reducer as formReducer } from 'redux-form';
import Root from './root';
import LoggedInReducer from './reducers/loginReducer';
import AuthenticateReducer from './reducers/authenticate';
import AccountingPeriodReducer from './reducers/accountingPeriodReducer';
import InvoiceReducer from './reducers/invoiceReducer';

const enhancer = composeWithDevTools(
  applyMiddleware(),
  install(),
);

const backendUrl = API_URL;

const initialState = {
  LoggedInReducer: { backendUrl },
  AuthenticateReducer: { backendUrl },
  InvoiceReducer: { backendUrl },
};
const store = createStore(
  combineReducers({
    form: formReducer,
    LoggedInReducer,
    AuthenticateReducer,
    AccountingPeriodReducer,
    InvoiceReducer,
  }),
  initialState,
  enhancer,
);

render(
  <Root store={store} />,
  document.getElementById('root'),
);
