import { Cmd, loop } from 'redux-loop';
import fetch from 'isomorphic-fetch';
import base64 from 'base-64';
import { AUTHENTICATE, authenticateFailure, authenticateSuccess } from '../actions/actions';

const authenticateUser = (backendUrl, user, password) => fetch(
  `${backendUrl}/customer`, {
    method: 'GET',
    headers: { Authorization: `Basic ${base64.encode(`${user}:${password}`)}` },
  },
).then(response => (response.ok ? Promise.resolve(response.json()) : Promise.reject(response)));

export default function reducer(state, action) {
  switch (action.type) {
    case AUTHENTICATE:
      return loop({
        ...state,
      },
      Cmd.run(
        authenticateUser, {
          successActionCreator: authenticateSuccess,
          failActionCreator: authenticateFailure,
          args: [
            state.backendUrl,
            action.user,
            action.password,
          ],
        },
      ));
    default:
      return state;
  }
}
