import { Cmd, loop } from 'redux-loop';
import fetch from 'isomorphic-fetch';
import {
  AUTHENTICATE_FAILURE,
  AUTHENTICATE_SUCCESS,
  LOGGED_IN,
  LOGGED_IN_FAILURE,
  LOGGED_IN_SUCCESS,
  loggedInFailure,
  loggedInSuccess,
  LOGOUT,
} from '../actions/actions';
import { DASHBOARD, LOGIN_FAILURE } from '../constants/routes';
import history from '../constants/history';

function navigateTo(route) {
  history.push(route);
}

const fetchLoginStatus = backendUrl => fetch(`${backendUrl}/customer`)
  .then(response => (response.ok ? Promise.resolve(response) : Promise.reject(response)));

export default function reducer(state = {}, action) {
  switch (action.type) {
    case LOGGED_IN:
      return loop({
        ...state,
        isLoggedIn: action.isLoggedIn,
        isCheckingLogon: action.isCheckingLogon,
      },
      Cmd.run(
        fetchLoginStatus, {
          successActionCreator: loggedInSuccess,
          failActionCreator: loggedInFailure,
          args: [state.backendUrl],
        },
      ));
    case LOGGED_IN_SUCCESS:
      return {
        ...state,
        isLoggedIn: action.isLoggedIn,
        isCheckingLogon: action.isCheckingLogon,
      };
    case LOGGED_IN_FAILURE:
      return {
        ...state,
        isLoggedIn: action.isLoggedIn,
        isCheckingLogon: action.isCheckingLogon,
      };
    case AUTHENTICATE_SUCCESS:
      return loop({
        ...state,
        isLoggedIn: true,
        user: action.user,
      },
      Cmd.run(
        navigateTo, {
          args: [
            DASHBOARD,
          ],
        },
      ));
    case AUTHENTICATE_FAILURE:
      return loop({
        ...state,
        isLoggedIn: false,
        error: action.error,
      },
      Cmd.run(
        navigateTo, {
          args: [
            LOGIN_FAILURE,
          ],
        },
      ));
    case LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        user: undefined,
      };
    default:
      return state;
  }
}
