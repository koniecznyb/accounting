import { Cmd, loop } from 'redux-loop';
import fetch from 'isomorphic-fetch';
import {
  FETCH_INVOICES,
  FETCH_INVOICES_SUCCESS,
  FETCH_INVOICES_FAILURE,
  fetchInvoicesSuccess,
  fetchInvoicesFailure,
  SELECT_ACCOUNTING_PERIOD,
} from '../actions/actions';

const [currentYear, currentMonth] = new Date().toISOString().split('T')[0].split('-');

const fetchInvoices = async (orchestrationUrl, selectedDate = `${currentYear}-${currentMonth}`) => {
  const [year, month] = selectedDate.split('-');
  const response = await fetch(`${orchestrationUrl}/invoices?year=${year}&month=${month}`);
  return response.ok
    ? response.json()
    : Promise.reject(response.statusText);
};

export default function reducer(state = {}, action) {
  switch (action.type) {
    case FETCH_INVOICES:
    case SELECT_ACCOUNTING_PERIOD:
      return loop({
        ...state,
      }, Cmd.run(
        fetchInvoices, {
          successActionCreator: fetchInvoicesSuccess,
          failActionCreator: fetchInvoicesFailure,
          args: [state.orchestrationUrl, action.selectedDate],
        },
      ));
    case FETCH_INVOICES_SUCCESS:
      return { ...state, invoices: action.invoices };
    case FETCH_INVOICES_FAILURE:
      return { ...state, error: action.error };
    default:
      return {
        ...state,
      };
  }
}
