export const INIT = 'INIT';
export const initAction = () => ({ type: INIT });

export const LOGGED_IN = 'LOGGED_IN';
export const loggedIn = () => ({
  type: LOGGED_IN,
  isLoggedIn: false,
  isCheckingLogon: true,
});

export const LOGGED_IN_SUCCESS = 'LOGGED_IN_SUCCESS';
export const loggedInSuccess = user => ({
  type: LOGGED_IN_SUCCESS,
  user,
  isLoggedIn: true,
  isCheckingLogon: false,
});

export const LOGGED_IN_FAILURE = 'LOGGED_IN_FAILURE';
export const loggedInFailure = error => ({
  type: LOGGED_IN_FAILURE,
  error,
  isLoggedIn: false,
  isCheckingLogon: false,
});

export const AUTHENTICATE = 'AUTHENTICATE';
export const authenticate = (user, password) => ({
  type: AUTHENTICATE,
  user,
  password,
});

export const AUTHENTICATE_SUCCESS = 'AUTHENTICATE_SUCCESS';
export const authenticateSuccess = user => ({
  type: AUTHENTICATE_SUCCESS,
  user,
});

export const AUTHENTICATE_FAILURE = 'AUTHENTICATE_FAILURE';
export const authenticateFailure = error => ({
  type: AUTHENTICATE_FAILURE,
  error,
});

export const LOGOUT = 'LOGOUT';
export const logout = () => ({
  type: LOGOUT,
});

export const SELECT_ACCOUNTING_PERIOD = 'SELECT_ACCOUNTING_PERIOD';
export const selectAccountingPeriod = selectedDate => ({
  type: SELECT_ACCOUNTING_PERIOD,
  selectedDate,
});

export const GET_INVOICES = 'GET_INVOICES';
export const getInvoices = (month, year) => ({
  type: SELECT_ACCOUNTING_PERIOD,
  month,
  year,
});

export const FETCH_INVOICES = 'FETCH_INVOICES';
export const fetchInvoices = (month, year) => ({
  type: FETCH_INVOICES,
  month,
  year,
});

export const FETCH_INVOICES_SUCCESS = 'FETCH_INVOICES_SUCCESS';
export const fetchInvoicesSuccess = invoices => ({
  type: FETCH_INVOICES_SUCCESS,
  invoices,
});

export const FETCH_INVOICES_FAILURE = 'FETCH_INVOICES_FAILURE';
export const fetchInvoicesFailure = error => ({
  type: FETCH_INVOICES_FAILURE,
  error,
});
