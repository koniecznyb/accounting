const {
  getLogger, publishMessage, consumeMessage,
} = require('common-library');
const app = require('express')();
const https = require('https');
const http = require('http');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const shortid = require('shortid');

let logger;

const {
  SERVER_HOST: host,
  SERVER_PORT: port,
  FRONTEND_URL: frontendUrl,
} = process.env;

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', frontendUrl);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


const handleRequest = async (req, exchange) => {
  const requestId = shortid.generate();
  logger = getLogger('orchestration-service', requestId);
  logger.info(`${req.method} for ${req.url}`);

  const message = {
    method: req.method,
    body: req.body,
    requestId,
  };

  return publishMessage(exchange, JSON.stringify(message), logger);
};

const handler = (msg, res) => {
  const response = JSON.parse(msg.content.toString());
  logger.info(`Got back response ${response}`);
  res.send(response.data);
};

const handleResponse = async (res, exchange) => {
  consumeMessage(exchange, msg => handler(msg, res));
};

app.use(helmet());
app.post('/invoices', async (req, res) => {
  try {
    await handleRequest(req, 'invoice-creation-request');
    await handleResponse(res, 'invoice-creation-response');
  } catch (err) {
    logger.error(err.stack);
    res.status(500)
      .end();
  }
});
app.get('/invoices', (req, res) => handleRequest(req, res)('invoice'));
app.post('/proprietorships', (req, res) => handleRequest(req, res)('proprietorship-creation'));
app.get('/proprietorships', (req, res) => handleRequest(req, res)('proprietorship'));
app.get('/customer', (req, res) => handleRequest(req, res)('customer'));


const serverStartup = () => {
  http.createServer(app)
    .listen(port, host);
  https.createServer()
    .listen(443);
  getLogger()
    .info(`server listening on ${host}:${port}`);
};

serverStartup();
