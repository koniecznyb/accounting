.PHONY: all

all: frontend orchestration invoice proprietorship
push: frontend-push orchestration-push invoice-push proprietorship-push

frontend:
	@ cd ./accountingFrontend && \
	make

orchestration:
	@ cd ./orchestration-service && \
	make

authentication:
	@ cd ./authentication-service && \
	make

invoice:
	@ cd ./invoice-service && \
	make

proprietorship:
	@ cd ./proprietorship-service && \
	make


frontend-push:
	@ cd ./accountingFrontend && \
	make push

orchestration-push:
	@ cd ./orchestration-service && \
	make push

authentication-push:
	@ cd ./authentication-service && \
	make push

invoice-push:
	@ cd ./invoice-service && \
	make push

proprietorship-push:
	@ cd ./proprietorship-service && \
	make push