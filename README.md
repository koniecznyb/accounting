### Accounting

According to 12-factor app approach all the default configuration is production like with additional config files
for specific environments (development or staging).

Deployments are done using kubernetes clusters.

Defaults:
`docker-compose.yml`
For development:
`docker-compose-dev.yml`
Production config:
`docker-compose-prod.yml`

Kubernetes config files are generated from these inside kubernetes directory.

## Development with hot code reload

`cd docker`

Create local .env file with your example configuration

```
DATABASE_PASSWORD=q1w2e3r4
DATABASE_USER=accounting
DATABASE_NAME=accounting
DATABASE_PORT=5432
REDIS_PORT=6379

FRONTEND_HOST=0.0.0.0
FRONTEND_PORT=80

ORCHESTRATION_SERVICE_HOST=0.0.0.0
ORCHESTRATION_SERVICE_PORT=8080

REGISTRY=registry.gitlab.com/koniecznyb/accounting
```

Build the images:

`docker-compose -f docker-compose.yml -f docker-compose-dev.yml build`

Then, you can start the app with all the dependencies using docker command:

`docker-compose -f docker-compose.yml -f docker-compose-dev.yml up`


## Deployment

Deploy:
`docker stack deploy -c docker-compose.yml -c docker-compose-prod.yml up`
