const { publishMessage, consumeMessage, getLogger } = require('common-library');
const db = require('./db.js');

let logger;

const handler = async (msg) => {
  const response = JSON.parse(msg.content.toString());
  logger = getLogger('invoice-service', response.requestId);
  logger.info(`message received ${JSON.stringify(response.body)}`);
  let result;
  try {
    result = await db(logger)
      .saveInvoice(response.body);
  } catch (e) {
    console.log(e);
  }
  await publishMessage('invoice-creation-response', result, logger);
  return result;
};


const start = async (exchange) => {
  try {
    consumeMessage(exchange, handler);
  } catch (e) {
    publishMessage('invoice-creation-response', 'failed', logger);
    logger.error(e);
  }
};


start('invoice-creation-request');
