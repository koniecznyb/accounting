const pgp = require('pg-promise')();

const db = pgp(process.env.DB_URL);

const getInvoices = logger => async (req) => {
  const { month, year } = req.query;
  logger.info(`Getting all invoices for ${year}-${month}`);
  if (month == null || year == null) {
    return db.manyOrNone('SELECT * FROM invoice');
  }
  return db.manyOrNone(`SELECT * FROM invoice WHERE date_part('month', issue_date) = ${month} AND date_part('year', issue_date) = ${year};`);
};

const saveInvoice = logger => async (invoice) => {
  logger.info(`Adding new invoice entry: ${JSON.stringify(invoice)}`);
  return db.one(`INSERT INTO invoice values (
      nextval('"seq_invoice_id"'),
      current_date,
      current_date,
      ${invoice.sum},
      ${invoice.from_nip},
      ${invoice.to_nip},
      '${invoice.issue_date}' )`);
};


module.exports = logger => ({
  getInvoices: getInvoices(logger),
  saveInvoice: saveInvoice(logger),
});
